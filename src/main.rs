// SPDX-License-Identfier: MPL-2.0

#![deny(clippy::correctness, clippy::suspicious)]
#![warn(clippy::complexity, clippy::perf, clippy::style, clippy::pedantic)]

#[macro_use]
extern crate tracing;

mod cli;
mod config;
mod generator;
mod git;
mod gpg;
mod password;
mod password_data;
mod subcmds;

use std::rc::Rc;

use clap::Parser;
use color_eyre::{
    config::HookBuilder,
    eyre::{self, Context},
};
use dialoguer::{console::Term, theme::ColorfulTheme};
use once_cell::sync::Lazy;
use resolve_path::PathResolveExt;
use tracing::level_filters::LevelFilter;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

use crate::cli::{Args, Commands};

static TERM: Lazy<Term> = Lazy::new(Term::stderr);
static THEME: Lazy<ColorfulTheme> = Lazy::new(ColorfulTheme::default);

fn init_error_hooks() -> eyre::Result<()> {
    let (panic, error) = HookBuilder::default().into_hooks();

    let panic = panic.into_panic_hook();
    let error = error.into_eyre_hook();

    color_eyre::eyre::set_hook(Box::new(move |err| error(err)))
        .context("failed to set eyre hook")?;

    std::panic::set_hook(Box::new(move |info| panic(info)));

    Ok(())
}

fn init_tracing_subscriber() -> eyre::Result<()> {
    let env_filter = {
        let default = EnvFilter::from_default_env();

        #[cfg(debug_assertions)]
        {
            default.add_directive(LevelFilter::DEBUG.into())
        }

        #[cfg(not(debug_assertions))]
        {
            default.add_directive(LevelFilter::WARN.into())
        }
    };

    let subscriber = FmtSubscriber::builder()
        .with_env_filter(env_filter)
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("setting default subscriber failed")?;

    info!("Tracing Started");

    Ok(())
}

fn main() -> eyre::Result<()> {
    init_error_hooks()?;

    let args = Args::parse();

    ctrlc::set_handler(|| {
        TERM.show_cursor().expect("failed to show cursor on stderr");
    })
    .context("failed to set ctrl-c handler")?;

    if args.verbose {
        init_tracing_subscriber()?;
    }

    #[cfg(feature = "gen_manpages")]
    {
        use clap::CommandFactory as _;
        use std::io::Write as _;

        let cmd = Args::command();

        clap_mangen::Man::new(cmd.clone())
            .generate_to(&format!("{0}/man", env!("CARGO_MANIFEST_DIR")))
            .context("failed to generate pass man pages")?;

        for subcmd in cmd.get_subcommands() {
            let man = clap_mangen::Man::new(subcmd.clone());

            let filepath = format!(
                "{0}/man/pass-{1}",
                env!("CARGO_MANIFEST_DIR"),
                man.get_filename()
            );
            let mut file = std::fs::File::create(&filepath)?;

            man.render(&mut file)?;

            file.flush()?;
        }

        panic!("Feature: gen_manpages enabled. Not continuing");
    }

    let store: Rc<str> = {
        let tmp = args.store.as_ref();

        let resolve = tmp.resolve().to_path_buf();

        resolve.to_string_lossy().into()
    };

    let store: &str = store.as_ref();

    match args.command {
        Commands::Config => subcmds::config::run(store)?,
        Commands::Init(args) => subcmds::init::run(store, &args)?,
        Commands::New(args) => subcmds::new::run(store, &args)?,
        Commands::Get(args) => subcmds::get::run(store, &args)?,
        Commands::Remove(args) => subcmds::remove::run(store, &args)?,
        Commands::Copy(args) => subcmds::copy::run(store, &args)?,
        Commands::Move(args) => subcmds::rename::run(store, &args)?,
        Commands::Edit(args) => subcmds::edit::run(store, &args)?,
        Commands::List(args) => subcmds::list::run(store, &args)?,
        Commands::Search(args) => subcmds::search::run(store, &args)?,
        Commands::Backup(args) => subcmds::backup::run(store, &args)?,
        Commands::Git(args) => subcmds::git::run(store, &args)?,
        Commands::Generate(args) => subcmds::completions::run(store, &args)?,
    }

    Ok(())
}
