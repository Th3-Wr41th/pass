// SPDX-License-Identfier: MPL-2.0

use std::{env, rc::Rc, sync::Arc};

use clap::{Parser, Subcommand, ValueEnum};
use resolve_path::PathResolveExt;

use crate::subcmds::{
    backup::BackupArgs, completions::CompletionArgs, copy::CopyArgs, edit::EditArgs, get::GetArgs,
    git::GitArgs, init::InitArgs, list::ListArgs, new::NewArgs, remove::RemoveArgs,
    rename::RenameArgs, search::SearchArgs,
};

/// Command line password manager. Based on and heavily inspired by password-store
#[derive(Debug, Clone, Parser)]
#[command(version, about, long_about = None)]
pub struct Args {
    /// Path to store default
    #[arg(short, long, default_value_t = Args::default_store())]
    pub store: Arc<str>,

    /// Show log output
    #[arg(short, long)]
    pub verbose: bool,

    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Commands {
    /// Initialize a new password store
    Init(InitArgs),

    /// Open config in default editor
    Config,

    /// Create new password
    New(NewArgs),

    /// Edit password, default is to open password in default editor
    Edit(EditArgs),

    /// List passwords (alias: ls)
    #[command(name = "list", alias = "ls")]
    List(ListArgs),

    /// Show passwords
    Get(GetArgs),

    /// Remove password (alias: rm)
    #[command(name = "remove", alias = "rm")]
    Remove(RemoveArgs),

    /// Copy password (alias: cp)
    #[command(name = "copy", alias = "cp")]
    Copy(CopyArgs),

    /// Move/Rename account (alias: mv)
    #[command(name = "move", alias = "mv")]
    Move(RenameArgs),

    /// Search for account
    Search(SearchArgs),

    /// Backup password store
    Backup(BackupArgs),

    /// Call git on the password store
    Git(GitArgs),

    /// Generate shell completions
    #[command(hide = true)]
    Generate(CompletionArgs),
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, ValueEnum)]
pub enum GenerateType {
    Password,
    #[default]
    Passphrase,
}

impl Args {
    fn default_store() -> Arc<str> {
        let dir: Rc<str> = if let Some(var) = env::var_os("XDG_DATA_HOME") {
            var.to_string_lossy().into()
        } else {
            let dir = match env::var("HOME") {
                Ok(val) => format!("{val}/.local/share"),
                Err(_err) => "~/.local/share".resolve().to_string_lossy().into(),
            };

            dir.into()
        };

        format!("{dir}/password-store").into()
    }
}
