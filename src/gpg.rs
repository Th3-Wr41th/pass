// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, bail, Context as _};
use gpgme::{Context, IntoData, SignatureSummary};
use serde::{Deserialize, Serialize};
use std::{rc::Rc, sync::Arc};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Gpg {
    pub protocol: Protocol,
    pub keys: Arc<[Arc<str>]>,
    pub signing_key: Option<Arc<str>>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Protocol {
    OpenPgp,
    Cms,
}

impl From<Protocol> for gpgme::Protocol {
    fn from(value: Protocol) -> Self {
        match value {
            Protocol::OpenPgp => Self::OpenPgp,
            Protocol::Cms => Self::Cms,
        }
    }
}

impl Gpg {
    pub fn encrypt<'a, I: IntoData<'a>>(&self, data: I) -> eyre::Result<Vec<u8>> {
        info!("creating gpg context");
        let mut ctx =
            Context::from_protocol(self.protocol.into()).context("failed to create gpg context")?;
        ctx.set_armor(true);

        info!("finding encryption keys");
        let keys: Vec<gpgme::Key> = if self.keys.is_empty() {
            warn!("no keys defined in config file");

            Vec::new()
        } else {
            let keys = self.keys.as_ref().iter().map(AsRef::as_ref);

            let iter = ctx.find_keys(keys).context("unable to find keys")?;

            iter.filter_map(Result::ok)
                .filter(gpgme::Key::can_encrypt)
                .collect()
        };

        if keys.is_empty() {
            bail!("cannot find keys capable of encryption in config");
        }

        let mut sign: bool = false;

        if let Some(signing_key) = &self.signing_key {
            let sign_key = ctx
                .get_key(signing_key.as_ref())
                .context("failed to get signing key")?;

            ctx.add_signer(&sign_key)
                .context("failed to add signing key as signer")?;

            sign = true;
        }

        let mut output: Vec<u8> = Vec::new();

        let encryption_res = if sign {
            let (encryption_res, _sign_res) = ctx
                .sign_and_encrypt(&keys, data, &mut output)
                .context("failed to sign and encrypt data")?;

            encryption_res
        } else {
            ctx.encrypt(&keys, data, &mut output)
                .context("failed to encrypt data")?
        };

        for invalid_key in encryption_res.invalid_recipients() {
            warn!(
                "invalid key: {0}",
                invalid_key.fingerprint().unwrap_or("[invalid fingerprint]")
            );
            warn!(
                "reason: {0}",
                invalid_key
                    .reason()
                    .map_or("[no reason found]".into(), |err| err.to_string())
            );
        }

        Ok(output)
    }

    pub fn decrypt<'a, I: IntoData<'a>>(&self, ciphertext: I) -> eyre::Result<Vec<u8>> {
        info!("creating gpg context");
        let mut ctx =
            Context::from_protocol(self.protocol.into()).context("failed to create gpg context")?;
        ctx.set_armor(true);

        let mut plaintext: Vec<u8> = Vec::new();

        let (_decrypt_res, verification_res) = ctx
            .decrypt_and_verify(ciphertext, &mut plaintext)
            .context("failed to decrypt data")?;

        info!("decrypted with following signatures:");
        for (i, sig) in verification_res.signatures().enumerate() {
            let indent = "  ";
            info!("sig {i}:");

            info!(
                "{indent}fingerprint: {0}",
                sig.fingerprint().unwrap_or("[invalid fingerprint]")
            );

            if let Err(err) = sig.status() {
                match err {
                    gpgme::Error::NO_ERROR => info!("good signature"),
                    gpgme::Error::BAD_SIGNATURE => warn!("bad signature"),
                    gpgme::Error::SIG_EXPIRED => warn!("signature expired"),
                    gpgme::Error::KEY_EXPIRED => warn!("key expired"),
                    gpgme::Error::CERT_REVOKED => warn!("key revoked"),
                    gpgme::Error::NO_PUBKEY => warn!("missing key"),
                    gpgme::Error::GENERAL => warn!("unknown error"),
                    _ => (),
                }
            }

            info!("{indent}validity: {0:?}", sig.validity());

            info!("{indent}summary:{0}", self.fmt_summary(sig.summary()));
        }

        Ok(plaintext)
    }

    #[allow(clippy::unused_self)]
    fn fmt_summary(&self, summary: SignatureSummary) -> Rc<str> {
        let mut s = String::new();

        if summary.contains(SignatureSummary::VALID) {
            s.push_str(" valid");
        }
        if summary.contains(SignatureSummary::GREEN) {
            s.push_str(" green");
        }
        if summary.contains(SignatureSummary::RED) {
            s.push_str(" red");
        }
        if summary.contains(SignatureSummary::KEY_REVOKED) {
            s.push_str(" revoked");
        }
        if summary.contains(SignatureSummary::KEY_EXPIRED) {
            s.push_str(" key-expired");
        }
        if summary.contains(SignatureSummary::SIG_EXPIRED) {
            s.push_str(" sig-expired");
        }
        if summary.contains(SignatureSummary::KEY_MISSING) {
            s.push_str(" key-missing");
        }
        if summary.contains(SignatureSummary::CRL_MISSING) {
            s.push_str(" crl-missing");
        }
        if summary.contains(SignatureSummary::CRL_TOO_OLD) {
            s.push_str(" crl-too-old");
        }
        if summary.contains(SignatureSummary::BAD_POLICY) {
            s.push_str(" bad-policy");
        }
        if summary.contains(SignatureSummary::SYS_ERROR) {
            s.push_str(" sys-error");
        }

        s.into()
    }
}
