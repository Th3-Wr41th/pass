// SPDX-License-Identfier: MPL-2.0

use crate::{cli::GenerateType, generator::Generator, TERM, THEME};
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::{Confirm, Password};
use passwords::{analyzer::analyze, scorer::score};
use std::sync::Arc;

pub fn manual_entry() -> eyre::Result<Arc<str>> {
    info!("prompting for password");

    let password = Password::with_theme(&*THEME)
        .with_prompt("Password")
        .with_confirmation("Repeat password", "The passwords don't match.")
        .validate_with(|input: &String| -> Result<(), &str> {
            let analyzed = analyze(input);

            let score = score(&analyzed);

            debug!("score: {score}");

            if score < 85.0 {
                let allow = Confirm::with_theme(&*THEME)
                    .with_prompt("Weak password, confirm to allow")
                    .default(false)
                    .report(false)
                    .interact_on(&TERM)
                    .unwrap_or(false);

                if allow {
                    Ok(())
                } else {
                    Err("Password is too weak")
                }
            } else {
                Ok(())
            }
        })
        .report(false)
        .interact_on(&TERM)
        .context("failed to get password from user")?;

    Ok(password.into())
}

pub fn generate(generate: GenerateType, generator: &Option<Generator>) -> eyre::Result<Arc<str>> {
    let generator: Generator = if let Some(gen) = &generator {
        gen.clone()
    } else {
        Generator::default()
    };

    info!("generating password");

    let pass = match generate {
        GenerateType::Password => {
            let (pass, score) = generator
                .generate_password()
                .context("failed to generate password")?;

            if score < 85.0 {
                let allow = Confirm::with_theme(&*THEME)
                    .with_prompt("Generated weak password, confirm to allow")
                    .default(false)
                    .report(false)
                    .interact_on(&TERM)
                    .unwrap_or(false);

                if !allow {
                    bail!("Weak password, check config options");
                }
            }

            pass
        }
        GenerateType::Passphrase => {
            let (pass, entropy) = generator.generate_passphrase();

            if entropy < 85.0 {
                let allow = Confirm::with_theme(&*THEME)
                    .with_prompt("Generated weak password, confirm to allow")
                    .default(false)
                    .report(false)
                    .interact_on(&TERM)
                    .unwrap_or(false);

                if !allow {
                    bail!("Weak password, check config options");
                }
            }

            pass
        }
    };

    Ok(pass)
}
