// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, Context as _};
use std::{collections::HashMap, fs, path::Path, sync::Arc};

use crate::config::Config;

#[derive(Debug, Clone)]
pub struct PasswordData(HashMap<Arc<str>, Arc<str>>);

impl PasswordData {
    pub fn new(password: &str) -> Self {
        Self(HashMap::from([("password".into(), password.into())]))
    }

    pub fn add_field(&mut self, name: Arc<str>, value: Arc<str>) {
        self.0.insert(name, value);
    }

    pub fn get_field(&self, key: &Arc<str>) -> Option<Arc<str>> {
        self.0.get(key).cloned()
    }

    pub fn as_str(&self) -> Option<Arc<str>> {
        toml::to_string_pretty(&self.0).ok().map(Into::into)
    }

    pub fn write<P: AsRef<Path>>(&self, config: &Config, path: P) -> eyre::Result<()> {
        info!("serializing password data");
        let toml_data = toml::to_string_pretty(&self.0).context("failed to serialize data")?;

        info!("encrypting password data");
        let output = config
            .gpg
            .encrypt(toml_data)
            .context("failed to encrypt password data")?;

        info!("writing data to file");

        fs::write(path, output).context("failed to write password data to file")?;

        Ok(())
    }

    pub fn from_file<P: AsRef<Path>>(config: &Config, path: P) -> eyre::Result<Self> {
        info!("reading cipertext");

        let ciphertext = fs::read_to_string(&path).context("failed to read account file")?;

        info!("decrypting");

        let output: Vec<u8> = config
            .gpg
            .decrypt(ciphertext)
            .context("failed to decrypt data")?;

        let plain =
            String::from_utf8(output).context("decrypted data contained non utf8 characters")?;

        let toml_data: HashMap<Arc<str>, Arc<str>> =
            toml::from_str(&plain).context("failed to deserialze data")?;

        Ok(Self(toml_data))
    }

    pub fn replace(&mut self, updated: &str) -> eyre::Result<()> {
        let toml_data: HashMap<Arc<str>, Arc<str>> =
            toml::from_str(updated).context("failed to deserialze data")?;

        self.0 = toml_data;

        Ok(())
    }
}
