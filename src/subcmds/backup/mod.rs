// SPDX-License-Identfier: MPL-2.0

mod remote_host;

use chrono::{DateTime, Utc};
use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use flate2::{write::GzEncoder, Compression};
use fs_extra::dir::CopyOptions;
use resolve_path::PathResolveExt as _;
use std::{
    env,
    fs::{self, File},
    path::{Path, PathBuf},
    process::Command,
    rc::Rc,
    sync::Arc,
};
use which::which;

use self::remote_host::RemoteHost;

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct BackupArgs {
    /// Do not compress store before copying
    #[arg(short, long)]
    pub plain: bool,

    /// Copy to remote server
    #[arg(long)]
    pub remote: Option<Arc<str>>,

    /// Force backup replacing existing files
    #[arg(short, long)]
    pub force: bool,

    /// Where to place the backup
    #[arg(default_value_t = Into::into("."))]
    pub output_path: Arc<str>,
}

pub fn run(store: &str, args: &BackupArgs) -> eyre::Result<()> {
    if let Some(remote) = &args.remote {
        info!("checking for scp");
        let bin = which("scp").context("cannot find scp binary")?;

        info!("parsing and validating remote");
        let remote_host = RemoteHost::parse(remote.as_ref()).context("failed to parse remote")?;

        let remote = remote_host.to_scp_fmt();

        if args.plain {
            warn!("copying plain is not recommend: see README.md");

            let (host, remote) = remote.as_ref().split_once(':').unwrap();

            let mut remote_path = PathBuf::from(remote);

            let dir_name: Rc<str> = match Path::new(store).file_name() {
                Some(name) => name.to_string_lossy().into(),
                None => bail!("Could not determine store directory name"),
            };

            let now: DateTime<Utc> = Utc::now();

            let timestamp = format!("{}", now.format("%Y%m%d"));

            remote_path.push(&format!("{dir_name}-{timestamp}"));

            info!("copying store to remote host");
            let status = Command::new(bin)
                .arg(&format!("-P {0}", remote_host.port))
                .arg("-Cpr")
                .arg(store)
                .arg(&format!("{host}:{0}", remote_path.to_string_lossy()))
                .status()
                .map_or(false, |exit| exit.success());

            if !status {
                bail!("failed to copy to remote host");
            }
        } else {
            let output_path = env::temp_dir();

            let filename = create_compressed_archive(store, output_path.clone(), args.force)
                .context("failed to create compressed archive")?;

            info!("copying compress archive to remote host");
            let status = Command::new(bin)
                .arg(&format!("-P {0}", remote_host.port))
                .arg("-Cp")
                .arg(&filename)
                .arg(remote.as_ref())
                .status()
                .map_or(false, |exit| exit.success());

            if !status {
                bail!("failed to copy archive to remote host");
            }

            info!("cleaning up");
            fs::remove_file(filename).context("failed to remove archive for temp dir")?;
        }
    } else {
        let output: PathBuf = args.output_path.as_ref().resolve().to_path_buf();

        if !output.exists() {
            bail!("Output path does not exist.");
        }

        if args.plain {
            let _ = copy(store, output.clone(), args.force)?;
        } else {
            let _ = create_compressed_archive(store, output.clone(), args.force)
                .context("failed to create compressed archive")?;
        }
    }

    Ok(())
}

fn create_compressed_archive(
    store: &str,
    mut output: PathBuf,
    force: bool,
) -> eyre::Result<PathBuf> {
    info!("creating compressed archive");

    let dir_name: Rc<str> = match Path::new(store).file_name() {
        Some(name) => name.to_string_lossy().into(),
        None => bail!("Could not determine store directory name"),
    };

    let now: DateTime<Utc> = Utc::now();

    let timestamp = format!("{}", now.format("%Y%m%d"));

    output.push(&format!("{dir_name}-{timestamp}.tar.gz"));

    if output.exists() {
        if !force {
            bail!("Archive already exists not overwriting");
        }

        info!("removing existing archive");

        fs::remove_file(&output).context("failed to remove existing archive")?;
    }

    info!("creating archive");
    let file = File::create(&output).context("failed to create archive file")?;

    let enc = GzEncoder::new(file, Compression::default());

    let mut tar = tar::Builder::new(enc);

    tar.append_dir_all(dir_name.as_ref(), store)
        .context("failed to append directory to archive")?;

    tar.finish().context("failed to finish writing archive")?;

    Ok(output)
}

fn copy(store: &str, mut output: PathBuf, force: bool) -> eyre::Result<PathBuf> {
    info!("creating plain backup");

    let dir_name: Rc<str> = match Path::new(store).file_name() {
        Some(name) => name.to_string_lossy().into(),
        None => bail!("Could not determine store directory name"),
    };

    let now: DateTime<Utc> = Utc::now();

    let timestamp = format!("{}", now.format("%Y%m%d"));

    output.push(&format!("{dir_name}-{timestamp}"));

    if output.exists() {
        if !force {
            bail!("Archive already exists not overwriting");
        }

        info!("removing existing archive");

        fs::remove_dir_all(&output).context("failed to remove existing directory")?;
    }

    info!("copying '{store:#?}' to '{output:#?}'");
    fs_extra::dir::copy(store, &output, &CopyOptions::new().content_only(true))
        .context("failed to copy directory")?;

    Ok(output)
}
