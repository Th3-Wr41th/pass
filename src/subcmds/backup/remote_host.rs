// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, bail, eyre, Context as _};
use idna::domain_to_ascii;
use once_cell::sync::Lazy;
use regex::Regex;
use std::net::IpAddr;
use std::str::FromStr;
use std::sync::Arc;

macro_rules! lazy_regex {
    ($re:literal) => {
        Lazy::new(|| Regex::new($re).expect("invalid regex"))
    };
}

static REMOTE_RE: Lazy<Regex> = lazy_regex!(
    r"^(?<user>.*?)@(?<host>[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*|(?:[a-fA-F0-9:\.]+)):(?<path>(?:(?:\/|~\/)(?:[A-z0-9-_+]+\/)*(?:[A-z0-9]+)*)|~|\/)$"
);

static ALT_REMOTE_RE: Lazy<Regex> = lazy_regex!(
    r"^(?<user>.*?)@(?:\[(?:(?<host>(?:[a-fA-F0-9:\.]+)|(?:.*?)):(?<port>.+)\])):(?<path>(?:(?:\/|~\/)(?:[A-z0-9-_+]+\/)*(?:[A-z0-9]+)*)|~|\/)$"
);

static USER_RE: Lazy<Regex> = lazy_regex!(r"^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$");

static DOMAIN_RE: Lazy<Regex> = lazy_regex!(
    r"^[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
);

#[derive(Debug, Clone)]
pub struct RemoteHost {
    pub user: Arc<str>,
    pub host: Arc<str>,
    pub port: Arc<str>,
    pub path: Arc<str>,
}

impl RemoteHost {
    pub fn to_scp_fmt(&self) -> Arc<str> {
        let Self {
            user, host, path, ..
        } = self;

        format!("{user}@{host}:{path}").into()
    }

    pub fn parse(remote: &str) -> eyre::Result<Self> {
        let s = if let Some(captures) = REMOTE_RE.captures(remote) {
            let user = &captures["user"];
            let host = &captures["host"];
            let port = "22";
            let path = &captures["path"];

            Ok(Self {
                user: user.into(),
                host: host.into(),
                port: port.into(),
                path: path.into(),
            })
        } else if let Some(captures) = ALT_REMOTE_RE.captures(remote) {
            let user = &captures["user"];
            let host = &captures["host"];
            let port = &captures["port"];
            let path = &captures["path"];

            Ok(Self {
                user: user.into(),
                host: host.into(),
                port: port.into(),
                path: path.into(),
            })
        } else {
            Err(eyre!("invalid remote format"))
        }?;

        s.validate()?;

        Ok(s)
    }

    fn validate(&self) -> eyre::Result<()> {
        let Self {
            user, host, port, ..
        } = self;

        if user.chars().count() > 64 {
            bail!("invalid user component: to many characters");
        }

        if !USER_RE.is_match(user) {
            bail!("invalid user component: contains invaild characters");
        }

        if host.chars().count() > 255 {
            bail!("invalid host component: to many characters");
        }

        match domain_to_ascii(host) {
            Ok(domain) => self.validate_domain(&domain)?,
            Err(err) => bail!("invalid host component: {err}"),
        }

        let port_range = 1023..=u16::MAX;

        let port_num: u16 = port
            .as_ref()
            .parse()
            .context("invalid port component: not valid u16")?;

        if port_num != 22u16 && !port_range.contains(&port_num) {
            bail!("invalid port component: port outside of valid range");
        }

        Ok(())
    }

    #[allow(clippy::unused_self)]
    fn validate_domain(&self, domain: &str) -> eyre::Result<()> {
        let is_ip = IpAddr::from_str(domain).is_ok();

        if is_ip {
            return Ok(());
        }

        if !DOMAIN_RE.is_match(domain) {
            bail!("invalid host component: domain contains invalid characters");
        }

        if domain.ends_with(|c: char| c.is_ascii_digit()) {
            bail!("invaild host component: domains cannot end with digits");
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use rstest::rstest;

    #[rstest]
    // Path Tests
    #[case("user@192.168.0.1:~", true)]
    #[case("user@192.168.0.1:/", true)]
    #[case("user@192.168.0.1:~/dir", true)]
    #[case("user@192.168.0.1:/root/dir", true)]
    // File tests
    #[case("user@192.168.0.1:~/file.invalid", false)]
    #[case("user@192.168.0.1:/file.invalid", false)]
    #[case("user@192.168.0.1:/nested/file.invalid", false)]
    #[case("user@192.168.0.1:file.invalid", false)]
    #[case("user@192.168.0.1:invalid", false)]
    // User tests
    #[case("user@192.168.0.1:~", true)]
    #[case("use1@192.168.0.1:~", true)]
    #[case("u-_r@192.168.0.1:~", true)]
    #[case("use$@192.168.0.1:~", true)]
    #[case("-use@192.168.0.1:~", false)]
    #[case("User@192.168.0.1:~", false)]
    #[case("USER@192.168.0.1:~", false)]
    // Host tests
    #[case("user@192.168.0.1:~", true)]
    #[case("user@1.1.1.1:~", true)]
    #[case("user@420.1.1.1:~", false)]
    #[case("user@example.com:~", true)]
    #[case("user@example.1a:~", true)]
    #[case("user@1.a:~", true)]
    #[case("user@example.1:~", false)]
    #[case("user@1.1.1:~", false)]
    #[case("user@*.example.com:~", false)]
    #[case("user@2001::0:1:~", true)]
    #[case("user@::1:~", true)]
    #[case("user@::::1:~", false)]
    // Port tests
    #[case("user@[192.168.0.1:22]:~", true)]
    #[case("user@[192.168.0.1:5022]:~", true)]
    #[case("user@[192.168.0.1:123456]:~", false)]
    #[case("user@[192.168.0.1:1]:~", false)]
    #[case("user@[192.168.0.1:0]:~", false)]
    #[case("user@[::1:22]:~", true)]
    #[case("user@[2001::0:1:22]:~", true)]
    #[case("user@[example.com:22]:~", true)]
    fn validate_remote(#[case] remote: &str, #[case] expected_validity: bool) {
        let res = RemoteHost::parse(remote);

        assert_eq!(res.is_ok(), expected_validity);
    }
}
