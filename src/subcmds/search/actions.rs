// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre;
use std::{fmt, rc::Rc, sync::Arc};

use crate::subcmds::{
    edit::{self, EditArgs},
    get::{self, GetArgs},
};

pub trait VecActionsExt {
    fn display(&self) -> Vec<Rc<str>>;
}

impl VecActionsExt for Vec<Actions> {
    fn display(&self) -> Vec<Rc<str>> {
        self.iter().map(Actions::name).collect()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Actions {
    Edit,
    Clipboard,
    QRCode,
    Plaintext,
    Nothing,
}

impl fmt::Display for Actions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name())
    }
}

impl Actions {
    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn name(&self) -> Rc<str> {
        match self {
            Self::Edit => "Edit password",
            Self::Clipboard => "Copy to clipboard",
            Self::QRCode => "Generate QR code",
            Self::Plaintext => "Show in plaintext",
            Self::Nothing => "Do nothing",
        }
        .into()
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn run(&self, store: &str, account: Arc<str>) -> eyre::Result<()> {
        match self {
            Self::Edit => edit::run(
                store,
                &EditArgs {
                    regenerate: None,
                    manual: false,
                    account,
                },
            ),
            Self::Clipboard => get::run(
                store,
                &GetArgs {
                    qr: false,
                    plain: false,
                    field: None,
                    account,
                },
            ),
            Self::QRCode => get::run(
                store,
                &GetArgs {
                    qr: true,
                    plain: false,
                    field: None,
                    account,
                },
            ),
            Self::Plaintext => get::run(
                store,
                &GetArgs {
                    qr: false,
                    plain: true,
                    field: None,
                    account,
                },
            ),
            Self::Nothing => {
                info!("nothing selected, not continuing");

                Ok(())
            }
        }
    }
}
