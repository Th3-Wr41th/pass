// SPDX-License-Identfier: MPL-2.0

mod actions;

use std::{fs, rc::Rc, sync::Arc};

use chrono::{DateTime, Utc};
use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::{FuzzySelect, Select};
use walkdir::{DirEntry, WalkDir};

use crate::{TERM, THEME};

use self::actions::{Actions, VecActionsExt};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct SearchArgs {
    /// Name or directory to filter results
    pub filter: Option<Arc<str>>,
}

pub fn run(store: &str, args: &SearchArgs) -> eyre::Result<()> {
    let dir_iter = WalkDir::new(store)
        .min_depth(0)
        .sort_by_key(|a| a.file_name().to_owned());

    let is_hidden = |entry: &DirEntry| -> bool {
        entry
            .file_name()
            .to_str()
            .is_some_and(|s| s.starts_with('.'))
    };

    let is_config = |entry: &DirEntry| -> bool {
        entry
            .file_name()
            .to_str()
            .is_some_and(|s| s.contains("config.toml"))
    };

    let accounts: Vec<Rc<str>> = dir_iter
        .into_iter()
        .filter_entry(|entry| !(is_hidden(entry) || is_config(entry)))
        .filter_map(Result::ok)
        .filter(|entry| {
            if let Some(filter) = &args.filter {
                let name = entry
                    .file_name()
                    .to_string_lossy()
                    .contains(filter.as_ref());

                name && !entry.file_type().is_dir()
            } else {
                !entry.file_type().is_dir()
            }
        })
        .map(|entry| -> Rc<str> { entry.into_path().to_string_lossy().into() })
        .filter_map(|entry| entry.strip_prefix(&format!("{store}/")).map(Into::into))
        .collect();

    if accounts.is_empty() {
        bail!("could not find accounts containing the provided filter");
    }

    let selected = FuzzySelect::with_theme(&*THEME)
        .with_prompt("Select Account")
        .default(0)
        .items(&accounts[..])
        .interact_on_opt(&TERM)
        .context("failed to get selection from user")?;

    let account = if let Some(idx) = selected {
        &accounts[idx]
    } else {
        info!("nothing selected, not continuing");

        return Ok(());
    };

    print_metadata(store, account.as_ref())?;

    let actions: Vec<Actions> = vec![
        Actions::Edit,
        Actions::Clipboard,
        Actions::QRCode,
        Actions::Plaintext,
        Actions::Nothing,
    ];

    let selected = Select::with_theme(&*THEME)
        .with_prompt("What would you like to do")
        .default(0)
        .items(&actions.display())
        .interact_on_opt(&TERM)
        .context("failed to get selection from user")?;

    if let Some(idx) = selected {
        actions[idx]
            .run(store, account.as_ref().into())
            .context("failed to run action")?;
    } else {
        info!("nothing selected, not continuing");

        return Ok(());
    }

    Ok(())
}

fn print_metadata(store: &str, account: &str) -> eyre::Result<()> {
    let path = format!("{store}/{account}");

    let metadata = fs::metadata(path).context("failed to get metadata for account: {account}")?;

    let timeformat = "%H:%M %d %b %Y";

    let created: Rc<str> = match metadata.created() {
        Ok(time) => {
            let datetime: DateTime<Utc> = time.into();

            format!("{}", datetime.format(timeformat)).into()
        }
        Err(_err) => "unknown".into(),
    };

    let edited: Rc<str> = match metadata.modified() {
        Ok(time) => {
            let datetime: DateTime<Utc> = time.into();

            format!("{}", datetime.format(timeformat)).into()
        }
        Err(_err) => "unknown".into(),
    };

    let created_prompt = format!(
        "{0} {1}",
        THEME.active_item_prefix,
        THEME
            .prompt_style
            .clone()
            .for_stderr()
            .apply_to(&format!("Created: {created}"))
    );

    let edited_prompt = format!(
        "{0} {1}",
        THEME.active_item_prefix,
        THEME
            .prompt_style
            .clone()
            .for_stderr()
            .apply_to(&format!("Edited: {edited}"))
    );

    TERM.write_line(&created_prompt)
        .context("failed to write created prompt to terminal")?;

    TERM.flush()?;

    TERM.write_line(&edited_prompt)
        .context("failed to write edited prompt to terminal")?;

    TERM.flush()?;

    TERM.clear_line()?;

    Ok(())
}
