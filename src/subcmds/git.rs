// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, Context};
use std::{process::Command, sync::Arc};
use which::which;

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct GitArgs {
    args: Vec<Arc<str>>,
}

pub fn run(store: &str, args: &GitArgs) -> eyre::Result<()> {
    let bin = which("git").context("could not find git binary")?;

    let status = Command::new(bin)
        .arg("-C")
        .arg(store)
        .args(args.args.iter().map(AsRef::as_ref))
        .status()
        .context("git command failed to start")?;

    info!(
        "git exited with code: {0}",
        status.code().map_or("[?]".into(), |code| format!("{code}"))
    );

    Ok(())
}
