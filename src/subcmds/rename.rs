// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::Confirm;
use git2::Repository;
use std::{fs, path::Path, sync::Arc};

use crate::{
    git::{Operation, RepositoryExt},
    TERM, THEME,
};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct RenameArgs {
    /// Name of account to move
    pub from: Arc<str>,

    /// Other account name
    pub to: Arc<str>,
}

pub fn run(store: &str, args: &RenameArgs) -> eyre::Result<()> {
    let from = format!("{store}/{0}", args.from);
    let to = format!("{store}/{0}", args.to);

    if !Path::new(&from).exists() {
        bail!("cannot find account with that name");
    }

    'root: {
        info!("checking destination");
        let path = Path::new(&to);

        if path.exists() {
            let allow = Confirm::with_theme(&*THEME)
                .with_prompt("Account already exists, override?")
                .default(false)
                .report(false)
                .interact_on(&TERM)
                .unwrap_or(false);

            if !allow {
                return Ok(());
            }
        } else {
            let Some(root) = path.parent() else {
                break 'root;
            };

            if !root.exists() {
                info!("creating required directories");

                fs::create_dir_all(root).context("failed to create directories for account")?;
            }
        }
    }

    info!("moving {0} to {1}", args.from, args.to);
    fs::rename(&from, to).context("failed to move file")?;

    info!("committing changes");

    let repo = Repository::open(store).context("failed to open store repository")?;

    repo.commit_changes(
        &[
            (args.from.as_ref(), Operation::Remove),
            (args.to.as_ref(), Operation::Add),
        ],
        &format!("move: {0} -> {1}", args.from, args.to),
    )
    .context("failed to commit changes")?;

    Ok(())
}
