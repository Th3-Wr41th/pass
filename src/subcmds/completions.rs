// SPDX-License-Identfier: MPL-2.0

use std::io;

use clap::{Args, CommandFactory};
use clap_complete::{generate, Shell};
use color_eyre::eyre;

use crate::cli::Args as CliArgs;

#[derive(Debug, Clone, Args)]
pub struct CompletionArgs {
    #[arg(value_enum)]
    shell: Shell,
}

#[allow(clippy::unnecessary_wraps)]
pub fn run(_store: &str, args: &CompletionArgs) -> eyre::Result<()> {
    let CompletionArgs { shell } = args;

    let mut cmd = CliArgs::command();
    let name = cmd.get_name().to_string();

    generate(*shell, &mut cmd, name, &mut io::stdout());

    Ok(())
}
