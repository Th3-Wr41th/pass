// SPDX-License-Identfier: MPL-2.0

use std::{env, ffi::OsStr, io, process::Stdio};

use color_eyre::eyre::{self, eyre, Context as _};

pub fn run(store: &str) -> eyre::Result<()> {
    let path = format!("{store}/config.toml");

    try_open(path).context("failed to open file")?;

    Ok(())
}

pub fn try_open<P: AsRef<OsStr>>(path: P) -> eyre::Result<()> {
    if let Some(val) = env::var_os("EDITOR") {
        let mut cmd = open::with_command(path, val.to_string_lossy());

        let status = cmd
            .stdin(Stdio::inherit())
            .stdout(io::stdout())
            .stderr(Stdio::null())
            .status();

        status.context("failed to open config")?;
    } else {
        let mut last_err: Option<eyre::Result<()>> = None;

        for mut cmd in open::commands(path) {
            let status = cmd
                .stdin(Stdio::inherit())
                .stdout(io::stdout())
                .stderr(Stdio::null())
                .status();

            match status {
                Ok(status) if status.success() => return Ok(()),
                Ok(status) => {
                    last_err = Some(Err(eyre!("Launcher {cmd:?} failed with {status:?}")));
                }
                Err(err) => last_err = Some(Err(eyre!(err))),
            }
        }

        if let Some(err) = last_err {
            err.context("No laucher worked, at least one error")?;
        }
    }

    Ok(())
}
