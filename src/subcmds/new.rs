// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, Context as _};
use dialoguer::Confirm;
use git2::Repository;
use std::{fs, path::Path, sync::Arc};

use crate::{
    cli::GenerateType,
    config::Config,
    git::{Operation, RepositoryExt},
    password,
    password_data::PasswordData,
    TERM, THEME,
};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct NewArgs {
    /// Generate password
    #[arg(long, value_enum, group = "type", default_value_t)]
    pub generate: GenerateType,
    /// Manually enter password
    #[arg(long, group = "type")]
    pub manual: bool,
    /// Name of account with optional sub directories
    pub account: Arc<str>,
}

pub fn run(store: &str, args: &NewArgs) -> eyre::Result<()> {
    let path = format!("{store}/{0}", args.account);

    'root: {
        info!("checking path");
        let path = Path::new(&path);

        if path.exists() {
            let allow = Confirm::with_theme(&*THEME)
                .with_prompt("Account already exists, override?")
                .default(false)
                .report(false)
                .interact_on(&TERM)
                .unwrap_or(false);

            if !allow {
                return Ok(());
            }
        } else {
            let Some(root) = path.parent() else {
                break 'root;
            };

            if !root.exists() {
                info!("creating required directories");

                fs::create_dir_all(root).context("failed to create directories for account")?;
            }
        }
    }

    info!("loading config");
    let config = Config::load(store).context("failed to load load config from file")?;

    let password: Arc<str> = if args.manual {
        password::manual_entry()?
    } else {
        password::generate(args.generate, &config.generator)?
    };

    let password_data = PasswordData::new(password.as_ref());

    password_data
        .write(&config, path)
        .context("failed to write password data")?;

    info!("committing changes");

    let repo = Repository::open(store).context("failed to open store repository")?;

    repo.commit_changes(
        &[(args.account.as_ref(), Operation::Add)],
        &format!("new: {0}", args.account),
    )?;

    info!("created new: {0}", args.account);

    Ok(())
}
