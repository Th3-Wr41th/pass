// SPDX-License-Identfier: MPL-2.0

use arboard::Clipboard;
use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::console::style;
use qrcode::{render::unicode, QrCode};
use std::{path::Path, sync::Arc, thread, time::Duration};

use crate::{config::Config, password_data::PasswordData, TERM};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct GetArgs {
    /// Print as a qr code
    #[arg(long, group = "display")]
    pub qr: bool,
    /// Show in plain text (not recommended)
    #[arg(long, group = "display")]
    pub plain: bool,
    /// Retrieve data from another field
    #[arg(short, long)]
    pub field: Option<Arc<str>>,
    /// Name of account with optional sub directories
    pub account: Arc<str>,
}

pub fn run(store: &str, args: &GetArgs) -> eyre::Result<()> {
    let path = format!("{store}/{0}", args.account);

    info!("checking path");
    if !Path::new(&path).exists() {
        bail!("Cannot find account with that name");
    }

    info!("loading config");
    let config = Config::load(store).context("failed to load load config from file")?;

    let password_data =
        PasswordData::from_file(&config, path).context("failed to read account file")?;

    let plain = if let Some(field) = &args.field {
        let Some(plain) = password_data.get_field(field) else {
            bail!("account file does contain the provided field");
        };

        plain
    } else {
        let Some(plain) = password_data.get_field(&"password".into()) else {
            bail!("account file does contain password field");
        };

        plain
    };

    if args.qr {
        info!("generating qr code");
        let code = QrCode::new(plain.as_ref()).context("failed to construct qr code")?;

        let string = code.render::<unicode::Dense1x2>().quiet_zone(true).build();

        println!("{string}");

        return Ok(());
    }

    if args.plain {
        warn!("printing password as plain text");

        println!("{plain}");

        return Ok(());
    }

    let mut clipboard = Clipboard::new().context("failed to create clipboard")?;

    info!("clearing clipboard");
    clipboard.clear().context("failed to clear clipboard")?;

    info!("copying password to clipboard");

    clipboard
        .set_text(plain.as_ref())
        .context("failed to put password on the clipboard")?;

    info!("clearing terminal in 10 seconds");
    for i in (0..10).rev() {
        if i != 9 {
            TERM.clear_last_lines(1)?;
        }

        TERM.write_line(&format!(
            "Clearing clipboard in {} seconds",
            style(i + 1).cyan(),
        ))
        .context("failed to write line to terminal")?;

        thread::sleep(Duration::from_millis(1000));
    }

    clipboard.clear().context("failed to clear clipboard")?;

    TERM.clear_last_lines(1)
        .context("failed to clear last line")?;

    Ok(())
}
