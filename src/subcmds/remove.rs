// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::Confirm;
use git2::Repository;
use std::{fs, path::Path, sync::Arc};

use crate::{
    git::{Operation, RepositoryExt},
    TERM, THEME,
};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct RemoveArgs {
    /// Name of account with optional sub directories
    pub account: Arc<str>,

    /// Force removal, disable confimation prompt
    #[arg(short, long)]
    pub force: bool,
}

pub fn run(store: &str, args: &RemoveArgs) -> eyre::Result<()> {
    let path = format!("{store}/{0}", args.account);

    info!("checking path");
    if !Path::new(&path).exists() {
        bail!("Cannot find account with that name");
    }

    if !args.force {
        info!("getting confimation from user");
        let confimation = Confirm::with_theme(&*THEME)
            .with_prompt("Are you sure")
            .default(false)
            .report(false)
            .interact_on(&TERM)
            .context("failed to get confirmation from user")?;

        if !confimation {
            info!("user declined to continue, exiting");

            return Ok(());
        }
    }

    info!("removing file");
    fs::remove_file(&path).context("failed to remove account")?;

    info!("committing changes");

    let repo = Repository::open(store).context("failed to open store repository")?;

    repo.commit_changes(
        &[(args.account.as_ref(), Operation::Remove)],
        &format!("remove: {}", args.account),
    )
    .context("failed to commit changes")?;

    Ok(())
}
