// SPDX-License-Identfier: MPL-2.0

use std::{path::Path, sync::Arc};

use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::Editor;
use git2::Repository;

use crate::{
    cli::GenerateType,
    config::Config,
    git::{Operation, RepositoryExt as _},
    password,
    password_data::PasswordData,
};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct EditArgs {
    /// Regenerate password
    #[arg(long, value_enum, group = "type")]
    pub regenerate: Option<GenerateType>,
    /// Manually enter new password
    #[arg(long, group = "type")]
    pub manual: bool,
    /// Name of account with optional sub directories
    pub account: Arc<str>,
}

pub fn run(store: &str, args: &EditArgs) -> eyre::Result<()> {
    let path = format!("{store}/{0}", args.account);

    info!("checking path");
    if !Path::new(&path).exists() {
        bail!("cannot find account with that name");
    }

    info!("loading config");
    let config = Config::load(store).context("failed to load load config from file")?;

    let mut password_data =
        PasswordData::from_file(&config, &path).context("failed to get password data from file")?;

    if args.manual {
        let pass = password::manual_entry()?;

        password_data.add_field("password".into(), pass);
    } else if let Some(regenerate) = args.regenerate {
        let pass = password::generate(regenerate, &config.generator)?;

        password_data.add_field("password".into(), pass);
    } else {
        let Some(plain) = password_data.as_str() else {
            bail!("could not deserialize password data");
        };

        let Some(updated) = Editor::new()
            .edit(plain.as_ref())
            .context("failed to edit password")?
        else {
            info!("password not changed");

            return Ok(());
        };

        password_data.replace(&updated)?;
    };

    password_data
        .write(&config, path)
        .context("failed to write password data")?;

    info!("committing changes");

    let repo = Repository::open(store).context("failed to open store repository")?;

    repo.commit_changes(
        &[(args.account.as_ref(), Operation::Add)],
        &format!("edit: {0}", args.account),
    )?;

    info!("edited: {0}", args.account);

    Ok(())
}
