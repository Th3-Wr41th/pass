// SPDX-License-Identfier: MPL-2.0

use dialoguer::console::style;
use std::{fmt::Write as _, rc::Rc};

use super::entry::{Entry, EntryType};

pub type NodeIndex = usize;

#[derive(Debug, Clone)]
pub struct Nodes(Vec<Node>);

impl Nodes {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn get_node(&self, idx: NodeIndex) -> Option<Node> {
        self.0.iter().find(|node| node.index == idx).cloned()
    }

    pub fn get_prev_node(&self) -> Option<Node> {
        self.0.last().cloned()
    }

    pub fn add_node(&mut self, node: Node) {
        let nodes = &mut self.0;

        nodes.push(node);

        self.0 = nodes.clone();
    }

    pub fn check_node(&self, node: &Node, entry: &Entry) -> Option<usize> {
        if node.entry.depth < entry.depth {
            return Some(node.index);
        }

        if node.entry.depth == entry.depth {
            return node.parent;
        }

        if let Some(parent_idx) = node.parent {
            let Some(node) = self.get_node(parent_idx) else {
                return None;
            };

            return self.check_node(&node, entry);
        }

        None
    }

    pub fn print(&self) {
        for node in &self.0 {
            node.print(self);
        }
    }
}

#[derive(Debug, Clone)]
pub struct Node {
    pub index: NodeIndex,
    pub parent: Option<NodeIndex>,

    pub entry: Entry,
}

impl Node {
    fn part(&self) -> TreePart {
        if self.entry.last {
            TreePart::Corner
        } else if self.parent.is_some() {
            TreePart::Edge
        } else {
            TreePart::Blank
        }
    }

    pub fn print(&self, nodes: &Nodes) {
        let name = match self.entry.entry_type {
            EntryType::File => style(self.entry.name.as_ref()),
            EntryType::Directory => style(self.entry.name.as_ref()).cyan().bold(),
        };

        let head: Rc<str> = 'head: {
            if self.index == 0 {
                break 'head "".into();
            }

            let mut head: Vec<TreePart> = vec![self.part()];

            #[allow(clippy::items_after_statements)]
            fn append_parent(nodes: &Nodes, node: &Node, head: &mut Vec<TreePart>) {
                let Some(parent) = node.parent else {
                    return;
                };

                if parent == 0 {
                    return;
                }

                let Some(node) = nodes.get_node(parent) else {
                    return;
                };

                if node.entry.last {
                    head.push(TreePart::Blank);
                } else {
                    head.push(TreePart::Line);
                }

                append_parent(nodes, &node, head);
            }

            append_parent(nodes, self, &mut head);

            head.iter()
                .rev()
                .fold(String::new(), |mut f, part| {
                    write!(f, "{}", style(part.ascii_art()).black())
                        .expect("failed to write to string");

                    f
                })
                .into()
        };

        if head.is_empty() {
            println!("{name}",);
        } else {
            println!("{head} {name}");
        }
    }
}

// src: eza::output::tree
#[derive(Debug, Clone, Copy)]
enum TreePart {
    /// Rightmost column, *not* the last in the directory.
    Edge,

    /// Not the rightmost column, and the directory has not finished yet.
    Line,

    /// Rightmost column, and the last in the directory.
    Corner,

    /// Not the rightmost column, and the directory *has* finished.
    Blank,
}

impl TreePart {
    /// Turn this tree part into ASCII-licious box drawing characters!
    /// (Warning: not actually ASCII)
    #[rustfmt::skip]
    pub fn ascii_art(self) -> &'static str {
        match self {
            Self::Edge    => "├──",
            Self::Line    => "│  ",
            Self::Corner  => "└──",
            Self::Blank   => "   ",
        }
    }
}
