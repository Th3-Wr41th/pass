// SPDX-License-Identfier: MPL-2.0

mod entry;
mod nodes;

use clap::Args;
use color_eyre::eyre::{self, bail};
use std::{cmp::Ordering, path::Path, sync::Arc};
use walkdir::{DirEntry, WalkDir};

use self::{
    entry::Entry,
    nodes::{Node, Nodes},
};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct ListArgs {
    /// Recurse into directories
    #[arg(short = 'R', long)]
    pub recurse: bool,
    /// Path to list (relative to password store)
    pub path: Option<Arc<str>>,
}

pub fn run(store: &str, args: &ListArgs) -> eyre::Result<()> {
    let path: Arc<str> = if let Some(path) = &args.path {
        format!("{store}/{path}").into()
    } else {
        store.into()
    };

    {
        info!("checking path");
        let path = Path::new(path.as_ref());

        if !path.exists() {
            bail!("cannot find directory or account with that name");
        }
    }

    let mut dir_iter = WalkDir::new(path.as_ref()).min_depth(0);

    // group directories first
    dir_iter = dir_iter.sort_by(|a, b| {
        if a.file_type().is_dir() {
            return Ordering::Less;
        }

        if b.file_type().is_dir() {
            return Ordering::Greater;
        }

        a.file_name().cmp(b.file_name())
    });

    if !args.recurse {
        dir_iter = dir_iter.max_depth(1);
    }

    let is_hidden = |entry: &DirEntry| -> bool {
        entry
            .file_name()
            .to_str()
            .is_some_and(|s| s.starts_with('.'))
    };

    let is_config = |entry: &DirEntry| -> bool {
        entry
            .file_name()
            .to_str()
            .is_some_and(|s| s.contains("config.toml"))
    };

    let mut walk_iter = dir_iter
        .into_iter()
        .filter_entry(|entry| !(is_hidden(entry) || is_config(entry)))
        .filter_map(Result::ok)
        .filter_map(Entry::parse)
        .enumerate()
        .peekable();

    let mut nodes: Nodes = Nodes::new();

    loop {
        let Some((idx, mut entry)) = walk_iter.next() else {
            break;
        };

        entry.last = if let Some((_, next)) = walk_iter.peek() {
            next.depth < entry.depth
        } else {
            true
        };

        if let Some(node) = nodes.get_prev_node() {
            let parent_idx = nodes.check_node(&node, &entry);

            let node = Node {
                index: idx,
                parent: parent_idx,

                entry,
            };

            nodes.add_node(node);
        } else {
            let node = Node {
                index: idx,
                parent: None,

                entry,
            };

            nodes.add_node(node);
        }
    }

    nodes.print();

    Ok(())
}
