// SPDX-License-Identfier: MPL-2.0

use std::sync::Arc;
use walkdir::DirEntry;

#[derive(Debug, Clone)]
pub struct Entry {
    pub name: Arc<str>,
    pub depth: usize,
    #[allow(clippy::struct_field_names)]
    pub entry_type: EntryType,
    pub last: bool,
}

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Copy)]
pub enum EntryType {
    Directory,
    File,
}

impl Entry {
    #[allow(clippy::needless_pass_by_value, clippy::unnecessary_wraps)]
    pub fn parse(dir_entry: DirEntry) -> Option<Self> {
        let file_name = dir_entry.file_name();
        let name: &str = &file_name.to_string_lossy();

        let depth = dir_entry.depth();

        let entry_type = if dir_entry.file_type().is_dir() {
            EntryType::Directory
        } else {
            EntryType::File
        };

        Some(Self {
            name: name.into(),
            depth,
            entry_type,
            last: false,
        })
    }
}
