// SPDX-License-Identfier: MPL-2.0

use std::{rc::Rc, sync::Arc};

use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::FuzzySelect;
use git2::Repository;
use gpgme::{Context as GpgContext, Key, Validity};

use crate::{
    config::Config,
    git::RepositoryExt,
    gpg::{Gpg, Protocol},
    TERM, THEME,
};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, Args)]
pub struct InitArgs {
    /// GPG keys to use for cryptographic operations
    ///
    /// If not provided will ask for key during operations when needed.
    pub gpgid: Vec<Arc<str>>,

    /// GPG key to use for signing account files.
    #[arg(short, long)]
    pub signing_key: Option<Arc<str>>,

    /// Do not use a signing key
    #[arg(long)]
    pub no_sign: bool,

    /// Use the cms protocol for crypotographic operations
    #[arg(long)]
    pub cms: bool,
}

pub fn run(store: &str, args: &InitArgs) -> eyre::Result<()> {
    let proto = if args.cms {
        Protocol::Cms
    } else {
        Protocol::OpenPgp
    };

    let (keys, signing_key): (Vec<Arc<str>>, Option<Arc<str>>) = if args.gpgid.is_empty() {
        (vec![], None)
    } else {
        get_gpgkey(proto, &args.gpgid, args.signing_key.clone(), args.no_sign)?
    };

    let config = Config {
        gpg: Gpg {
            protocol: proto,
            keys: keys.as_slice().into(),
            signing_key,
        },
        generator: None,
    };

    info!("initializing git repo in config dir");
    let repo = Repository::initialize(store)
        .context("failed to initialize git repository in config directory")?;

    info!("writing config");
    config.write(store)?;

    info!("creating initial commit");

    repo.initial_commit()
        .context("failed to create initial commit")?;

    info!("initialization complete");

    Ok(())
}

#[allow(clippy::type_complexity)]
fn get_gpgkey(
    proto: Protocol,
    ids: &[Arc<str>],
    signing_key: Option<Arc<str>>,
    no_sign: bool,
) -> eyre::Result<(Vec<Arc<str>>, Option<Arc<str>>)> {
    let mut ctx = GpgContext::from_protocol(proto.into()).context("failed to create context")?;

    let mut keys = ctx
        .find_keys(ids.iter().map(AsRef::as_ref))
        .context("failed to find keys")?;

    let ok_keys = keys
        .by_ref()
        .filter_map(Result::ok)
        .filter(Key::can_encrypt);

    let (gpg_keys, selections): (Vec<Key>, Vec<Rc<str>>) = ok_keys
        .map(|key| {
            let short_id = key.id().unwrap_or("?");
            let user = key.user_ids().next();

            let (user_id, validity) = if let Some(user) = user {
                (user.id().unwrap_or("Unknown"), user.validity())
            } else {
                ("Unknown", Validity::Unknown)
            };

            let validity = match validity {
                Validity::Unknown => "Unknown",
                Validity::Undefined => "Undefined",
                Validity::Never => "Never",
                Validity::Marginal => "Marginal",
                Validity::Full => "Full",
                Validity::Ultimate => "Ultimate",
            };

            let pretty: Rc<str> = format!("{user_id} [{short_id}] ({validity})").into();

            (key, pretty)
        })
        .unzip();

    if gpg_keys.is_empty() {
        bail!("cannot find key with that id");
    }

    if keys.finish()?.is_truncated() {
        bail!("Key listing unexpectedly truncated");
    }

    let gpg_key_ids: Vec<Arc<str>> = gpg_keys
        .iter()
        .filter_map(|key| key.id().ok())
        .map(Into::into)
        .collect();

    if no_sign {
        return Ok((gpg_key_ids, None));
    }

    let signing_key: Arc<Key> = if gpg_keys.len() > 1 && signing_key.is_none() {
        let selection = FuzzySelect::with_theme(&*THEME)
            .with_prompt("Select signing key")
            .default(0)
            .items(&selections)
            .report(false)
            .interact_on(&TERM)
            .context("failed to get selection from user")?;

        gpg_keys[selection].clone().into()
    } else if let Some(key) = signing_key {
        let mut ctx =
            GpgContext::from_protocol(proto.into()).context("failed to create context")?;

        let key = ctx
            .get_key(key.as_ref())
            .context(format!("cannot find key that name: {key}"))?;

        if !key.can_sign() {
            bail!("key invalid: cannot sign");
        }

        key.into()
    } else {
        gpg_keys[0].clone().into()
    };

    let Ok(signing_key_id) = signing_key.id() else {
        bail!("Invalid short id for requested key");
    };

    Ok((gpg_key_ids, Some(signing_key_id.into())))
}
