// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, Context as _};
use serde::{Deserialize, Serialize};
use std::{fs, path::Path};

use crate::{generator::Generator, gpg::Gpg};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub gpg: Gpg,
    pub generator: Option<Generator>,
}

impl Config {
    pub fn load(store: &str) -> eyre::Result<Self> {
        let path = format!("{store}/config.toml");

        let content = fs::read_to_string(path).context("failed to read config file")?;

        let toml: Self = toml::from_str(&content).context("failed to deserialize config data")?;

        Ok(toml)
    }

    pub fn write(&self, store: &str) -> eyre::Result<()> {
        info!("config file write requested");
        let path = format!("{store}/config.toml");

        'root: {
            info!("checking config path");
            let path = Path::new(&path);

            if !path.exists() {
                let Some(root) = path.parent() else {
                    break 'root;
                };

                if !root.exists() {
                    info!("creating config directory");

                    fs::create_dir_all(root)
                        .context("failed to create directories for config file")?;
                }
            }
        }

        info!("serializing config");
        let toml = toml::to_string_pretty(&self).context("failed to serialize config data")?;

        info!("writing config");
        fs::write(path, toml).context("failed to write config file")?;

        Ok(())
    }
}
