// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, eyre, Context as _, OptionExt as _};
use dialoguer::Confirm;
use git2::{Commit, IndexAddOption, Oid, Repository};
use gpgme::{Context, Key};
use std::{path::Path, rc::Rc};

use crate::{subcmds::config::try_open, TERM, THEME};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Operation {
    Add,
    AddAll,
    Remove,
}

pub trait RepositoryExt {
    fn initialize<P: AsRef<Path> + std::fmt::Debug>(path: P) -> eyre::Result<Repository>;

    fn initial_commit(&self) -> eyre::Result<()>;

    fn get_signing_key(&self) -> Option<Key>;

    fn create_signed_commit(
        &self,
        key: Key,
        message: &str,
        tree_id: Oid,
        parents: &[&Commit],
    ) -> eyre::Result<Oid>;

    fn create_commit(&self, message: &str, tree_id: Oid, parents: &[&Commit]) -> eyre::Result<Oid>;

    fn files<P: AsRef<Path>>(&self, paths: &[(P, Operation)]) -> eyre::Result<Oid>;

    fn commit_changes<P: AsRef<Path>>(
        &self,
        paths: &[(P, Operation)],
        message: &str,
    ) -> eyre::Result<()>;
}

impl RepositoryExt for Repository {
    fn initialize<P: AsRef<Path> + std::fmt::Debug>(path: P) -> eyre::Result<Repository> {
        let repo = Repository::init(&path).context("failed to init repo")?;

        let confirmation = Confirm::with_theme(&*THEME)
            .with_prompt("Do you want to edit the git config")
            .default(false)
            .report(false)
            .interact_on(&TERM)
            .context("failed to get confirmation from user")?;

        if confirmation {
            let path = path.as_ref().join(".git/config");

            try_open(path).context("failed to open gitconfig")?;
        }

        Ok(repo)
    }

    fn initial_commit(&self) -> eyre::Result<()> {
        let tree_id = self.files(&[("*", Operation::AddAll)])?;

        let config = self.config().context("unable to get config for git")?;

        let branch: Rc<str> = config
            .get_str("init.defaultBranch")
            .unwrap_or("master")
            .into();

        let message = "initial commit";

        let commit_id = match self.get_signing_key() {
            Some(key) => self.create_signed_commit(key, message, tree_id, &[]),
            None => self.create_commit(message, tree_id, &[]),
        }?;

        let commit = self
            .find_commit(commit_id)
            .context("cannot find initial commit in repo")?;

        self.branch(branch.as_ref(), &commit, false)
            .context("failed to create branch for initial commit")?;

        Ok(())
    }

    fn files<P: AsRef<Path>>(&self, paths: &[(P, Operation)]) -> eyre::Result<Oid> {
        let mut index = self
            .index()
            .context("failed to get index file for repository")?;

        for (path, operation) in paths {
            let path: &Path = path.as_ref();

            match operation {
                Operation::Add => index.add_path(path).context(format!(
                    "failed to add path to index: {0}",
                    path.to_string_lossy()
                ))?,
                Operation::AddAll => index
                    .add_all(path.iter(), IndexAddOption::DEFAULT, None)
                    .context("failed to add all files to index")?,
                Operation::Remove => index.remove_path(path).context(format!(
                    "failed to remove path from index: {0}",
                    path.to_string_lossy()
                ))?,
            }
        }

        index.write().context("failed to write index")?;

        index
            .write_tree()
            .context("failed to write index as a tree")
    }

    fn commit_changes<P: AsRef<Path>>(
        &self,
        paths: &[(P, Operation)],
        message: &str,
    ) -> eyre::Result<()> {
        let head = self.head()?;
        let branch = head.shorthand().unwrap_or("master");

        info!("finding parent commit");

        let head = self.head().context("unable to get HEAD of the repo")?;
        let head_oid = head
            .target()
            .ok_or_eyre(eyre!("HEAD does not appear to be a valid reference"))?;

        let head_commit = self
            .find_commit(head_oid)
            .context("cannot find commit referenced by HEAD")?;

        info!("adding files to index");

        let tree_id = self.files(paths)?;

        info!("creating commit");

        let commit_id = match self.get_signing_key() {
            Some(key) => self.create_signed_commit(key, message, tree_id, &[&head_commit])?,
            None => self.create_commit(message, tree_id, &[&head_commit])?,
        };

        self.reference(&format!("refs/heads/{branch}"), commit_id, true, message)
            .context("failed to create reference for commit")?;

        Ok(())
    }

    fn create_commit(&self, message: &str, tree_id: Oid, parents: &[&Commit]) -> eyre::Result<Oid> {
        let sig = self
            .signature()
            .context("failed to get signature for git repository")?;

        let tree = self.find_tree(tree_id).context("failed to find tree")?;

        self.commit(Some("HEAD"), &sig, &sig, message, &tree, parents)
            .context("failed to create commit")
    }

    fn create_signed_commit(
        &self,
        key: Key,
        message: &str,
        tree_id: Oid,
        parents: &[&Commit],
    ) -> eyre::Result<Oid> {
        let mut ctx = Context::from_protocol(gpgme::Protocol::OpenPgp)
            .context("failed to create gpg context for signed commit")?;

        let sig = self
            .signature()
            .context("failed to get signature for git repository")?;

        let tree = self.find_tree(tree_id).context("failed to find tree")?;

        let buf = self
            .commit_create_buffer(&sig, &sig, message, &tree, parents)
            .context("failed to create commit buffer")?;

        let contents = buf
            .as_str()
            .ok_or_eyre(eyre!("commit buffer is invalid utf-8"))?;
        let mut outbuf = Vec::new();

        ctx.add_signer(&key)
            .context("failed to add signing key to gpg context")?;
        ctx.set_armor(true);
        ctx.sign(gpgme::SignMode::Detached, contents, &mut outbuf)
            .context("failed to sign commit buffer")?;

        let out = std::str::from_utf8(&outbuf).context("signed commit is invalid utf-8")?;

        self.commit_signed(contents, out, None)
            .context("failed to create signed commit")
    }

    fn get_signing_key(&self) -> Option<Key> {
        let mut config = self.config().ok()?;

        let config = config.snapshot().ok()?;

        let sign: bool = config.get_bool("commit.gpgsign").ok()?;

        if !sign {
            return None;
        }

        let signing_key: Rc<str> = config.get_str("user.signingkey").ok()?.into();

        let mut ctx = Context::from_protocol(gpgme::Protocol::OpenPgp).ok()?;

        let keys = ctx.find_keys([signing_key.as_ref()]).ok()?;

        let key = keys.filter_map(Result::ok).find(Key::can_sign)?;

        Some(key)
    }
}
