// SPDX-License-Identfier: MPL-2.0

use chbs::{
    config::BasicConfig,
    scheme::ToScheme,
    word::{WordList, WordSampler},
};
use color_eyre::eyre::{self, eyre, Context};
use passwords::{analyzer::analyze, scorer::score, PasswordGenerator};
use serde::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Generator {
    #[serde(default = "PasswordRules::default")]
    pub password: PasswordRules,
    #[serde(default = "PassphraseRules::default")]
    pub passphrase: PassphraseRules,
}

impl Generator {
    pub fn generate_password(&self) -> eyre::Result<(Arc<str>, f64)> {
        let generator: PasswordGenerator = self.password.clone().into();

        let pass: Arc<str> = match generator.generate_one() {
            Ok(val) => val.into(),
            Err(err) => return Err(eyre!("{err}")).context("failed to create password"),
        };

        let analyzed = analyze(pass.as_ref());

        let score = score(&analyzed);

        Ok((pass, score))
    }

    pub fn generate_passphrase(&self) -> (Arc<str>, f64) {
        let generator: BasicConfig<WordSampler> = self.passphrase.clone().into();

        let scheme = generator.to_scheme();

        let entropy = scheme.entropy().bits();

        info!("Expected passphrase entropy: {entropy}");

        let pass = scheme.generate();

        (pass.into(), entropy)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PasswordRules {
    pub length: usize,
    pub strict: bool,
    pub exclude_similar_characters: bool,
    pub include: Arc<[Include]>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Include {
    Lowercase,
    Uppercase,
    Numbers,
    Symbols,
    Spaces,
}

impl Default for PasswordRules {
    fn default() -> Self {
        PasswordGenerator::default().into()
    }
}

impl From<PasswordRules> for PasswordGenerator {
    fn from(value: PasswordRules) -> Self {
        Self {
            length: value.length,
            numbers: value.include.contains(&Include::Numbers),
            lowercase_letters: value.include.contains(&Include::Lowercase),
            uppercase_letters: value.include.contains(&Include::Uppercase),
            symbols: value.include.contains(&Include::Symbols),
            spaces: value.include.contains(&Include::Spaces),
            exclude_similar_characters: value.exclude_similar_characters,
            strict: value.strict,
        }
    }
}

impl From<PasswordGenerator> for PasswordRules {
    fn from(value: PasswordGenerator) -> Self {
        let mut include: Vec<Include> = Vec::new();

        macro_rules! if_then {
            ($condition:expr, $val:expr) => {
                if $condition {
                    $val
                }
            };
        }

        if_then!(value.lowercase_letters, include.push(Include::Lowercase));
        if_then!(value.uppercase_letters, include.push(Include::Uppercase));
        if_then!(value.numbers, include.push(Include::Numbers));
        if_then!(value.symbols, include.push(Include::Symbols));
        if_then!(value.spaces, include.push(Include::Spaces));

        Self {
            length: value.length,
            strict: value.strict,
            exclude_similar_characters: value.exclude_similar_characters,
            include: include.as_slice().into(),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PassphraseRules {
    pub words: usize,
    pub separator: Arc<str>,
    pub capitalize_first: Probability,
    pub capitalize_words: Probability,
}

impl Default for PassphraseRules {
    fn default() -> Self {
        BasicConfig::default().into()
    }
}

impl From<PassphraseRules> for BasicConfig<WordSampler> {
    fn from(value: PassphraseRules) -> Self {
        Self {
            words: value.words,
            word_provider: WordList::default().sampler(),
            separator: value.separator.as_ref().into(),
            capitalize_first: value.capitalize_first.into(),
            capitalize_words: value.capitalize_words.into(),
        }
    }
}

impl From<BasicConfig<WordSampler>> for PassphraseRules {
    fn from(value: BasicConfig<WordSampler>) -> Self {
        Self {
            words: value.words,
            separator: value.separator.into(),
            capitalize_first: value.capitalize_first.into(),
            capitalize_words: value.capitalize_words.into(),
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[serde(tag = "when", content = "often", rename_all = "lowercase")]
pub enum Probability {
    Always,
    Sometimes(f64),
    Never,
}

impl Probability {
    pub fn clamp(self) -> Self {
        match self {
            Self::Sometimes(val) => {
                if val >= 1.0 {
                    Self::Always
                } else if val <= 0.0 {
                    Self::Never
                } else {
                    Self::Sometimes(val)
                }
            }
            other => other,
        }
    }
}

impl From<Probability> for chbs::probability::Probability {
    fn from(value: Probability) -> Self {
        let value = value.clamp();

        match value {
            Probability::Always => Self::Always,
            Probability::Sometimes(val) => Self::Sometimes(val),
            Probability::Never => Self::Never,
        }
    }
}

impl From<chbs::probability::Probability> for Probability {
    fn from(value: chbs::probability::Probability) -> Self {
        match value {
            chbs::probability::Probability::Always => Self::Always,
            chbs::probability::Probability::Sometimes(val) => Self::Sometimes(val),
            chbs::probability::Probability::Never => Self::Never,
        }
    }
}
