# pass

Command line password manager. Based on and heavily inspired by [password-store](https://www.passwordstore.org/)

## Requirements

This crate depends on the `gpgme` create which has its own [requirements](https://github.com/gpg-rs/gpgme)

## Structure

```
[store]
    - config.toml
    - [account]
    - [account]
    - [subdir]/[account]
    - [subdir]/[subdir]/[account]
```

```
[keys]
    - master password key (signing key)
    - store key (required)
    - [work accounts key] (optional)
    - [personal accounts key] (optional)
```

Store key can be the same as the master password key.

## Usage

See [usage](/docs/usage.md)

## Notes

`CHANGELOG.git.md` is built with [`changelog`](https://gitlab.com/Th3-Wr41th/changelog)

## License

This project is Licensed under the [Mozilla Public License 2.0](LICENSE) 
