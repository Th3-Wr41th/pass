# Using pass

## Setting it up

To begin, there is a single command to initialize the password store:

```sh
$ pass init demo@pass.project
```

Here, `demo@pass.project` is the ID of my GPG key. You can use your standard
GPG key or one especially for the password store. Multiple GPG keys can be
specified:

```sh
$ pass init demo@pass.project user@example.com
```

Account files can also be signed by providing the `-s` flag with a GPG key id:

```sh
$ pass init -s passwords@signing.key demo@pass.project
```

If a signing key is not provided you will be asked which one of the provided
keys to use as a signing key. Signing can be disabled outright by providing the
`--no-sign` flag. If you use the `cms` protocol instead of openpgp you can
provide the `--cms` flag to the switch to using the cms protocol.

Password stores are git repositories, each time the store is manipulated a new
commit will be created. During initialization you will be asked if you want to
configure git, this can be useful if you want to use a different user identity
for the password store, or enable/disable commit signing, for example:

```
$ pass init ...
? Do you want to edit the git config (y/n) > yes
```

```ini
...

[user]
    email = demo@pass.project
    name = demo
    signingkey = ...

[commit]
    gpgsign = true
```

## Configuration

Each store has it own config file, it can be accessed with the `config` command
which will open it in the default editor. An example config with explanations
of each entry can be found in `share/config.toml`

## Usage

We can list all the existing passwords in the store with the `ls` command:

```sh
$ pass ls
password-store
├── example
├── demo2
└── demo
```

to recursively expand subdirectories, provide the `-R` flag:

```sh
$ pass ls -R
password-store
├── example
│  └── demo
├── demo2
└── demo
```

We can list a specific subdirectory by providing the path as an argument:

```sh
$ pass ls example
example
└── demo
```

To get a password from the store we can use the `get` command, by default this
copies the password to the clipboard and clears after 10 seconds:

```sh
$ pass get example/demo
```

We can also show it as a qr code by providing the `--qr` flag:

```sh
$ pass get --qr example/demo


    █▀▀▀▀▀█  ▀ ▄▀ █▀▀▀▀▀█
    █ ███ █ █▀ ▀  █ ███ █
    █ ▀▀▀ █ █▄▀▄▀ █ ▀▀▀ █
    ▀▀▀▀▀▀▀ █▄▀ ▀ ▀▀▀▀▀▀▀
    █ █▀▀▀▀ ▄▀ ▀▄ ▀▀███ ▄
    ▀▀  ██▀█▄▀▄ ██▀  ██▀
    ▀▀▀▀  ▀ ▄██▀▀▀▀█▄▄ ▀▄
    █▀▀▀▀▀█ ▄▀▀▀▀█▀▄ ███
    █ ███ █ █▄█▄▀█▀▀▄▄ ▀▀
    █ ▀▀▀ █ ▀▀██▀█▀  ▀▄
    ▀▀▀▀▀▀▀ ▀▀ ▀  ▀ ▀  ▀


```

or if we simply want to print it in plain text, we can provide the `--plain`
flag:

```sh
$ pass get --plain example/demo
demo-password
```

To add a password to the store we can use the `new` command, by default it will
generate a passphrase for the account, based of the configuration of the rules
it may generate a weak password/passphrase and will require manual confirmation
to allow:

```sh
$ pass new example/demo
```

If you wish to generate as password instead:

```sh
$ pass new --generate=password example/demo
```

or to manually enter a password:

```sh
$ pass new --manual example/demo
```

To edit an existing password, we can use the `edit` command, by default this
will open the account file in the default editor, this can also be used to add
metadata such as email address to the account file:

```sh
$ pass edit example/demo
```

If you wish to regenerate a password or passphrase:

```sh
$ pass edit --regenerate=password example/demo
```

or manually enter a new password:

```sh
$ pass edit --manual example/demo
```

To search the password store, we can use the the `search` command. With no
arguments this will show a fuzzy searchable prompt to find an account:

```sh
$ pass search
? Select Account >
❯ example/demo
❯ demo2
❯ demo
```

You can provide a search filter as an argument, for example a specific directory
or name.

Upon selecting an account it will show some metadata for when it was created
and last edited and prompt to select an action:

```sh
$ pass search
✔ Select Account · example/demo
❯ Created: 23:06 15 Mar 2024
❯ Edited: 23:06 15 Mar 2024
? What would you like to do >
❯ ...
```

Account files can be removed the `remove` command, a confirmation prompt be
shown be it's removed, this can be disabled with the `-f` flag:

```sh
$ pass rm demo
? Are you sure (y/n) > no
```

To rename or move an account file, we can use the `move` command:

```sh
$ pass mv demo2 demo
```

To copy an account file, we can use the `copy` command:

```sh
$ pass cp demo example/demo2
```

Pass supports backing up of the password store either locally or to remote
servers using scp. A backup can either be a `.tar.gz` archive or a plain
directory, by default it create a compressed archive:

```sh
$ pass backup ~/backups
$ pass backup --plain ~/backups
```

for remote backups:

```sh
$ pass backup --remote "user@example.com:~/backups"
$ pass backup --remote "user@example.com:~/backups" --plain
```

Backing up as a plain repository is not recommend as may cause corruption to the 
file structure, espically when copying to a remote servers as scp (the built in
protocol for copying to remote servers) does not respect existing files or
directories, as well as taking up more space on disk (in testing with 5 passwords,
in excess of 2000% more).

As the password store is a git repository `pass` provides the `git` command
which will pass provided arguments to git and run it in the password store
directory.

## Account file metadata

Account files are serialzied with toml. When editing an account file with `edit`,
we can add metadata in the form of key value pairs:

```toml
# This field already exists
password = "demo-password"

# Metadata
email = "demo@example.org"
website = "example.com/login"
```

## Backup Remote Hosts

When backing up to a remote host there a several forms its allowed to be and
rules it has to follow. The host is broken down into four key parts:

```text
[user]@[[host]:[port]]:[path]
OR
[user]@[host]:[path]
```

For the `[user]` part the rules are as follows:

- Must start with a lowercase letter or an underscore
- Then it should be either
    - 0 to 31 characters of letters, numbers, underscores, and/or hyphens
    - OR 0 to 30 characters of the above plus a US dollar sign symbol at the end

The rules for the `[host]` part are as follows:

- A valid ipv4 or ipv6 address
- OR a vaild domain name
- AND no longer the 255 characters

The rules for the `[port]` part are as follows:

- is port 22
- OR is between 1024 and 65535

if a port is not provided, for example in the second form, it will default to
port `22`

For the `[path]` part the rules are as follows:

- Must be a directory
- Valid path
- start with either
    - `/`
    - `~`
