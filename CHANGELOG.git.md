# Changelog

All changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Documentation

- **1e4546e** - update manpages
- **ea3741a** - cleanup readme and expand usage docs in a separate file
- **215e89f** - update example config
- **a8eaaac** - add example config.toml
- **7e7ba39** - update changelog

### Code Refactoring

- **20de27c** - remove the error prefix from the confirmation error message
- **410584b** - apply clippy fixes
- **28393dc** - rename subcmd 'show' to 'get'
- **a165721** - move gpg related code to its own module
- **2f959ed** - move config subcmd to subcmd module

### Style

- **8d8ff15** - tidy up code

### Features

- **fd6bd46** - add default to remove confirmation
- **e69f771** - add support for generating shell completions
- **46e3956** - add support for executing git commands in the password store
- **c6e939b** - add support to initialize with multiple keys and optional signing key
- **d88f806** - add support retriving data from alternate fields
- **4f6f011** - add support for metadata in account file
- **90feb0a** - simplify struct for password rules

### Bug Fixes

- **b8575ae** - filter out directories when a filter is provided
- **5ee80c4** - remove the hide attribute from the remote arg
- **d2201af** - use correct method to add all files to the index
- **7c1aedd** - remove unnecessary call to show/hide cusor

## [v0.2.0] - 2024-03-14 

### Chores

- **4915798** - bump version to v0.2.0

### Documentation

- **db53e36** - fix examples and links in readme
- **93e94f1** - update manpages
- **2d4a9c5** - update changelog
- **bd2adf3** - update changelog
- **cf23819** - update readme
- **559da2b** - add feature to generate man pages
- **20ea6c6** - update changelog

### Style

- **4c3bb48** - format code with rustfmt

### Code Refactoring

- **b591988** - breakup list into seperate modules
- **3812b23** - apply clippy fixes and optimize dependency features
- **5ca097b** - split actions into a seperate module
- **6323c82** - rename 'backup.rs' to 'backup/mod.rs'
- **9f5db4a** - move password generation code to dedicated module

### Features

- **f044faf** - add check for account path before attempting to remove account file
- **b75e262** - add confirmation prompt before removing account file
- **0e557a4** - complete impl of 'backup' subcommand
- **887411d** - partially impl 'backup' subcommand
- **2da345d** - impl 'search' subcommand
- **f4f8c5b** - add visibility qualifiers to args structs
- **a7ab82f** - impl 'list' subcommand
- **4d04914** - impl 'edit' subcommand

### Bug Fixes

- **1a95fea** - remove used arg 'interactive'

## [v0.1.0] - 2024-03-09 

### Bug Fixes

- **a656609** - remove `todo!()` as feature is complete
- **d0b2c39** - include git module in module tree
- **931969c** - commits no longer ammend to the initial commit

### Features

- **120c63d** - use 'fs::rename' in place of 'fs::copy' and 'fs::remove_file'
- **d55b09f** - impl 'move' subcommand
- **f86b3e5** - impl 'copy' subcommand
- **6315f54** - impl 'remove' subcommand
- **0b92510** - impl 'show' subcommand
- **f1d95b3** - add menu to select key when many are avaliable
- **08f14b8** - impl 'new' subcommand
- **abbf847** - resolve store path to absolute path
- **add80e7** - return entropy/score alongside password
- **c0a0d88** - impl password/passphrase generator
- **0e59415** - impl init subcommand
- **e3d44da** - impl config structure and utility functions
- **f826ab9** - impl config subcommand
- **12e8dce** - scaffold command line interface
- **3d5a577** - init for errors hooks and tracing

### Other

- **ec78c89** - initial commit

### Code Refactoring

- **fb7c476** - move subcommands to dedicated module
- **0e19f57** - simplify process to commit changes
- **3314b4b** - allow add_all to take paths as input
- **a076a64** - move functionality for getting gpg key to separate function
- **95ba962** - move git functionality to an extension trait

### Documentation

- **fbc0b36** - add changelog
- **c3d7fab** - update notes

### Chores

- **3062599** - bump version to v0.1.0
- **bed889e** - add expected dependencies

[Unreleased]: https://gitlab.com/Th3-Wr41th/pass/compare/v0.2.0...HEAD
[v0.2.0]: https://gitlab.com/Th3-Wr41th/pass/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/Th3-Wr41th/pass/commits/v0.1.0
